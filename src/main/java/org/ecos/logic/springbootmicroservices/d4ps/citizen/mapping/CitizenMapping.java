package org.ecos.logic.springbootmicroservices.d4ps.citizen.mapping;

import org.ecos.logic.springbootmicroservices.d4ps.citizen.dto.CitizenRequest;
import org.ecos.logic.springbootmicroservices.d4ps.citizen.dto.CitizenResponse;
import org.ecos.logic.springbootmicroservices.d4ps.citizen.entity.Citizen;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CitizenMapping {

    @Mapping(source = "emailAddress", target = "email")
    CitizenResponse fromEntityToDto(Citizen expedient);

    List<CitizenResponse> fromEntityCollectionToDtoCollection(List<Citizen> citizenCollection);

    @Mapping(source = "email", target = "emailAddress")
    Citizen fromDtoRequestToEntity(CitizenRequest citizenRequest);
}
