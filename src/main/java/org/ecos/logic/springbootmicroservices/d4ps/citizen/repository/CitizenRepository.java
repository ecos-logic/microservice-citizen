package org.ecos.logic.springbootmicroservices.d4ps.citizen.repository;

import org.ecos.logic.springbootmicroservices.d4ps.citizen.entity.Citizen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CitizenRepository extends JpaRepository<Citizen,Long> {
}