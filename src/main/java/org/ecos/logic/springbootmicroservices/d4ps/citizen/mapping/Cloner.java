package org.ecos.logic.springbootmicroservices.d4ps.citizen.mapping;

import org.ecos.logic.springbootmicroservices.d4ps.citizen.dto.CitizenRequest;
import org.ecos.logic.springbootmicroservices.d4ps.citizen.entity.Citizen;

public class Cloner {
    public static Citizen cloneFromRequestToEntity(CitizenRequest citizenRequest, Citizen citizen) {
        citizen.setName(citizenRequest.getName());
        citizen.setSurname(citizenRequest.getSurname());
        citizen.setSecondSurname(citizenRequest.getSecondSurname());
        citizen.setTaxId(citizenRequest.getTaxId());
        citizen.setEmailAddress(citizenRequest.getEmail());
        return citizen;
    }
}
