package org.ecos.logic.springbootmicroservices.d4ps.citizen.controller;

import jakarta.validation.Valid;
import org.ecos.logic.springbootmicroservices.d4ps.citizen.dto.CitizenRequest;
import org.ecos.logic.springbootmicroservices.d4ps.citizen.dto.CitizenResponse;
import org.ecos.logic.springbootmicroservices.d4ps.citizen.entity.Citizen;
import org.ecos.logic.springbootmicroservices.d4ps.citizen.mapping.CitizenMapping;
import org.ecos.logic.springbootmicroservices.d4ps.citizen.mapping.Cloner;
import org.ecos.logic.springbootmicroservices.d4ps.citizen.repository.CitizenRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static java.lang.String.format;

@RestController
@RequestMapping("/citizen")
public class CitizenController {
    private final CitizenRepository repository;

    private final CitizenMapping mapping;

    public CitizenController(CitizenRepository repository, CitizenMapping mapping) {
        this.repository = repository;
        this.mapping = mapping;
    }

    @GetMapping("/")
    public ResponseEntity<List<CitizenResponse>> list(){
        List<Citizen> citizenCollection = this.repository.findAll();
        List<CitizenResponse> citizenResponseCollection =  this.mapping.fromEntityCollectionToDtoCollection(citizenCollection);
        return ResponseEntity.ok().body(citizenResponseCollection);
    }

    @GetMapping("/queryCitizen/{id}")
    public ResponseEntity<CitizenResponse> listBy(@PathVariable Long id){
        return repository.findById(id).
                map(citizen -> ResponseEntity.ok().body(this.mapping.fromEntityToDto(citizen))).
                orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteBy(@PathVariable Long id){
        return repository.
                findById(id).
                map(expedient ->{
                    this.repository.deleteById(id);
                    return ResponseEntity.noContent().build();
                }).
                orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/createCitizen")
    public ResponseEntity<?> create(@Valid @RequestBody CitizenRequest citizenRequest, BindingResult bindingResult){
        if(bindingResult.hasErrors()) {
            StringBuilder explanation = prepareErrorMessage(bindingResult);
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(explanation.toString());
        }
        Citizen citizen = this.mapping.fromDtoRequestToEntity(citizenRequest);

        Citizen savedCitizen = this.repository.save(citizen);

        CitizenResponse result = this.mapping.fromEntityToDto(savedCitizen);

        return ResponseEntity.status(HttpStatus.CREATED).body(result.getId());
    }

    @PutMapping("{id}")
    public ResponseEntity<?> update(@PathVariable Long id,@Valid @RequestBody CitizenRequest citizenRequest, BindingResult bindingResult){
        if(bindingResult.hasErrors()) {
            StringBuilder explanation = prepareErrorMessage(bindingResult);
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(explanation.toString());
        }

        Optional<Citizen> optionalCitizen = repository.findById(id);

        if(optionalCitizen.isEmpty())
            return ResponseEntity.notFound().build();


        Citizen savedCitizen = Cloner.cloneFromRequestToEntity(citizenRequest,optionalCitizen.get());


        this.repository.save(savedCitizen);

        CitizenResponse result = this.mapping.fromEntityToDto(savedCitizen);

        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    private static StringBuilder prepareErrorMessage(BindingResult bindingResult) {
        StringBuilder explanation = new StringBuilder();

        for (FieldError error: bindingResult.getFieldErrors()) {
            explanation.append(format("%s\t %s", error.getField(), error.getDefaultMessage())).append("\n");
        }
        return explanation;
    }

}
