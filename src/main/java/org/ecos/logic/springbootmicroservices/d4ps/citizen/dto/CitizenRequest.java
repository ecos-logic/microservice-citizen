package org.ecos.logic.springbootmicroservices.d4ps.citizen.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

@SuppressWarnings("unused")
public class CitizenRequest {
    @NotEmpty
    @Size(min = 1,max = 20)
    private String name;
    @NotEmpty
    @Size(min = 1,max = 30)
    private String surname;
    @NotEmpty
    @Size(min = 1,max = 30)
    private String secondSurname;
    @NotEmpty
    @Size(min = 1,max = 9)
    private String taxId;
    @NotEmpty
    @Size(min = 1,max = 40)
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
